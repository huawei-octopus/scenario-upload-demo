"""Bulk uploading logical scenarios to Octopus."""
import os
import hashlib
import json
import aiohttp
import asyncio
import time
import requests
from enum import Enum

from argparse import ArgumentParser
from dataclasses import dataclass
from pathlib import Path
from typing import Optional, List
from aiohttp import TCPConnector

token = ''
# Environment variable.
OSLSM_ENDPOINT = os.getenv('OSLSM_ENDPOINT', 'default')
OSSM_ENDPOINT = os.getenv('OSSM_ENDPOINT', 'default')
PROJECT_ID = os.getenv('PROJECT_ID', 'default')
VIAM = os.getenv('VIAM', 'default')
VERIFY_SSL = os.getenv('VERIFY_SSL', False)
MAX_TASK_SIZE = os.getenv('MAX_TASK_SIZE', 10)  # 异步上传时最多并行的任务数，不能设置太大，并发太大会导致创建场景失败


TOKEN_FILE = 'token.txt'

# Exception class
class ObusException(Exception):
    """Provide the base exception for obus."""


class UploadScenarioToOssmError(ObusException):
    """Upload scenario file to ossm error."""


class AssignedPathNotOneError(ObusException):
    """The assigned path of scenario files is not equal to one."""


# model
@dataclass
class ScenarioPath:
    """The path of scenario."""

    scenario: Optional[Path]
    map: Optional[Path]
    generalize: Optional[Path]

    def __bool__(self):
        return self.scenario is not None and self.map is not None


def main(argv=None):
    """Octopus Batch Upload Scenario."""
    parser = ArgumentParser()
    parser.add_argument('-V', '--version', action='version')
    parser.description = main.__doc__
    parser.set_defaults(func=lambda _: 1)
    subparsers = parser.add_subparsers(title='subcommands')
    add_parser_upload_to(subparsers)
    add_parser_login_to(subparsers)
    args = parser.parse_args(argv)
    status = args.func(args)
    raise SystemExit(status)


def add_parser_login_to(subparsers):
    """Add sub command login to main parser."""
    cmd: ArgumentParser = subparsers.add_parser(
        'login',
        aliases='l',
        help=login_from_cli.__doc__,
    )
    cmd.description = login_from_cli.__doc__
    cmd.set_defaults(func=login_from_cli)

    #认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    #本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。
    #如果进行了加密还需要进行解密操作。
    cmd.add_argument(
        '-ak',
        required=True,
        help='The access key of your account.'
    )
    cmd.add_argument(
        '-sk',
        required=True,
        help='The secret key of your account.'
    )


def login_from_cli(args) -> 0:
    """Login in platform with username and password."""
    _login(args.ak, args.sk)
    return 0


def _login(ak, sk):
    data = {'ak': ak, 'sk': sk}
    headers = {'Content-Type': 'application/json'}
    resp = requests.post(url=VIAM, json=data, verify=False, headers=headers)
    resp.raise_for_status()
    token_ = json.loads(resp.content)['accessToken']
    with open(TOKEN_FILE, 'w') as file:
        file.write(token_)


class Version(Enum):
    V1 = 'v1.1.1'
    V2 = 'v2.0.0'


def add_parser_upload_to(subparsers):
    """Add sub command upload to main parser."""
    cmd: ArgumentParser = subparsers.add_parser(
        'upload',
        aliases='u',
        help=upload_from_cli.__doc__,
    )
    cmd.description = upload_from_cli.__doc__
    cmd.set_defaults(func=upload_from_cli)

    cmd.add_argument(
        '--path',
        '-p',
        type=Path,
        nargs='+',
        help='The directory of scenario files.'
    )
    cmd.add_argument(
        '--root',
        '-r',
        default="batch_scenario",
        type=str,
        help='根节点名称，用作构建根场景库分类'
    )
    cmd.add_argument(
        '--version',
        '-v',
        default=Version.V1.value,
        choices=[Version.V1.value, Version.V2.value],
        help='The version of dynamic logical scenario files, default is v1.1.1.'
    )


def get_token():
    global token
    with open(TOKEN_FILE, 'r') as file:
        token = file.read()


def upload_from_cli(args) -> int:
    """Upload scenario files from cli command."""
    start = time.time()
    get_token()
    asyncio.run(_upload(args.path, args.version, args.root))
    print(f'total time is {time.time() - start}')
    return 0


def is_null(cur_dir):
    """判断当前文件夹是否为空文件夹."""
    children = []
    for item in cur_dir.iterdir():
        children.append(item)
    if len(children) == 0:
        return True
    return False


def is_scenario(cur_dir):
    """判断当前文件夹是否为场景，即当前文件夹下只有文件，没有其他文件夹."""
    for item in cur_dir.iterdir():
        if os.path.isdir(item):
            return False
    return True


def is_group(cur_dir):
    """判断当前文件夹是否为场景库，即当前文件夹下只有场景."""
    for item in cur_dir.iterdir():
        if not is_scenario(item):
            return False
    return True


async def _recursive_upload(cur_dir, category, group, version, count, tasks):
    if is_null(cur_dir):
        pass
    elif is_scenario(cur_dir):
        # create scenario
        if group is None:
            raise ValueError("Scenario can't be patched to category.")
        tasks.append(_upload_files(cur_dir, version, group))
        if len(tasks) == MAX_TASK_SIZE:
            res = await asyncio.gather(*tasks)
            count += _update_count(res)
            print(f'The uploaded scenario number is {count}')
            tasks.clear()
    elif is_group(cur_dir):
        # create group
        body = {"name": cur_dir.name, "parent": category}
        group = await create_group(body=body)
        for item in cur_dir.iterdir():
            count, tasks = await _recursive_upload(item, category, group, version, count, tasks)
    else:
        # create category
        body = {"name": cur_dir.name, "parent": category}
        category = await create_category(body=body)
        for item in cur_dir.iterdir():
            count, tasks = await _recursive_upload(item, category, group, version, count, tasks)

    return count, tasks


async def _upload(path: List[Path], version: str, root: str) -> int:
    """Upload scenario files."""
    # 判断root是否重名
    category_list = await get_category(params={"is_root": "true", "name": root})

    if category_list.get("count") > 0:
        raise ValueError(f"Root {root} already exists!")

    # 创建根场景库分类
    root_category = await create_category(body={"name": root})

    tasks = []
    for p in path:
        count, tasks = await _recursive_upload(p.absolute(), root_category, None, version, 0, tasks)

    if tasks:
        res = await asyncio.gather(*tasks)
        count += _update_count(res)
        print(f'The uploaded scenario number is {count}')
        tasks.clear()


def _update_count(res):
    count = 0
    for i in res:
        if i:
            count += 1
    return count


class FileType(Enum):
    OSC = 1
    XOSC = 2
    ODR = 3
    XODR = 4
    GENERALISE = 5  # 约定格式为**DeterministicParameterDistributionSet.xosc
    OTHER = 6


def get_file_type(version: str, f: Path):
    if version == Version.V1.value:
        if f.suffix == '.xodr':
            return FileType.XODR
        elif f.suffix == '.xosc':
            if 'DeterministicParameterDistributionSet' in f.name:
                return FileType.GENERALISE
            else:
                return FileType.XOSC
        else:
            return FileType.OTHER
    elif version == Version.V2.value:
        if f.suffix == '.odr':
            return FileType.ODR
        elif f.suffix == '.osc':
            return FileType.OSC
        elif f.suffix == '.xodr':
            return FileType.XODR
        else:
            return FileType.OTHER
    else:
        raise ValueError("Unknown version")


async def _upload_files(path: Path, version: str, group: int) -> bool:
    """Upload scenario files of a scenario directory."""
    scenario_path = ScenarioPath(None, None, None)
    scenario_fum, map_fnum, generalize_fnum = 0, 0, 0
    for child in path.iterdir():
        child_ftype = get_file_type(version, child)
        if child_ftype in [FileType.OSC, FileType.XOSC]:
            scenario_fum += 1
            scenario_path.scenario = child
        elif child_ftype in [FileType.XODR, FileType.ODR]:
            map_fnum += 1
            scenario_path.map = child
        elif child_ftype == FileType.GENERALISE:
            generalize_fnum += 1
            scenario_path.generalize = child
        
    # 校验文件夹中的文件数
    if version == Version.V1.value:
        if scenario_fum != 1 or map_fnum != 1 or generalize_fnum != 1:
            print(f'upload failed, {path.name} directory has unexcepted number of files.')
            return False
    elif version == Version.V1.value:
        if scenario_fum != 1 or map_fnum != 1 or generalize_fnum != 0:
            print(f'upload failed, {path.name} directory has unexcepted number of files.')
            return False
    else:
        raise ValueError('Unknown version')
    
    if scenario_path:
        print(f'uploading {scenario_path.scenario}')
        try:
            res = await upload_all_instance(
                path=scenario_path,
                project_id=PROJECT_ID,
                format='osc' if version==Version.V2 else 'xosc',
                type='user',
                description='',
                version=version,
                group=group
            )
            print(f'success upload {scenario_path.scenario}')
            return res
        except Exception as e:
            print(f'fail upload {scenario_path.scenario}, error: ', str(e)) 
            return False
    print(f'upload failed, {path.name} directory lack of necessary files.')
    return False


async def upload_all_instance(
    path: ScenarioPath, project_id, format, type, description, version, group
):
    """Upload all scenario instance."""
    map_url, map_status, _ = await upload_map(path.map, project_id)
    if not map_status:
        raise UploadScenarioToOssmError('Upload map to oslsm error')

    gen_url = None
    if version == Version.V1.value:
        gen_url, gen_status, _ = await upload_gen(path.generalize, project_id)
        if not gen_status:
            raise UploadScenarioToOssmError('Upload generalize to oslsm error')

    _, scenario_status, instance_id = await upload_scenario(
        path.scenario, path.map, path.generalize, project_id, map_url, gen_url,
        (format, type, description, version)
    )
    if scenario_status:
        if await update_scenario_status(project_id, instance_id):
            return await put_group(group, instance_id)
    raise UploadScenarioToOssmError('Upload scenario to oslsm error')


async def upload_scenario(path, map, gen, project_id, map_url, gen_url, detail):
    """Upload scenario."""
    scenario_url = OSLSM_ENDPOINT + f'/v2/{project_id}/sim/lsm/scenarios/'
    sha256 = get_sha256(path)
    body = {
        'format': detail[0],
        'type': detail[1],
        'description': detail[2],
        'file': {
            'sha256': sha256,
            'filename': path.name
        },
        'version': detail[3],
        'name': path.parent.name,
        'map': map_url,
        'map_filename': map.name
    }
    if gen_url:
        body['parameter'] = gen_url
        body['parameter_filename'] = gen.name
    return await upload_instance(scenario_url, body, path)


async def update_scenario_status(project_id, instance_id):
    """Update scenario status to available."""
    scenario_url = OSLSM_ENDPOINT + f'/v2/{project_id}/sim/lsm/scenarios/{instance_id}/'
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    body = {'status': 0}
    async with aiohttp.ClientSession() as sess:
        async with sess.patch(
                scenario_url, data=json.dumps(body), headers=headers, ssl=False
        ) as resp:
            return resp.status < 400


async def upload_file(path, put_url):
    """Upload scenario instance file."""
    headers = {'Content-Type': 'application/octet-stream'}
    with path.open('rb') as file:
        async with aiohttp.ClientSession() as sess:
            async with sess.put(
                put_url, data=file, headers=headers, ssl=False
            ) as resp:
                return resp.status < 400


async def upload_instance(instance_url, body, path):
    """Handle map or scenario."""
    instance_id, url, ready, put_url, sha256 = await create_instance(instance_url, body)

    if ready:
        return url, True, instance_id
    if not put_url:
        raise Exception('Ossm has some error')
    if await upload_file(path, put_url):
        status_url = instance_url + f'{instance_id}/files/{sha256}/'
        if await update_status(status_url):
            return url, True, instance_id
    return url, False, instance_id


async def upload_map(path, project_id):
    """Upload map."""
    if path.suffix == ".xodr":
        map_url = OSSM_ENDPOINT + f'/v2/{project_id}/sim/sm/maps/'
        format = "xodr"
    elif path.suffix == ".odr":
        map_url = OSLSM_ENDPOINT + f'/v2/{project_id}/sim/lsm/maps/'
        format = "odr"
    name, sha256 = path.name, get_sha256(path)
    body = {
        'file': {'sha256': sha256, 'filename': name}, 
        'version': 'v1.6.0', 
        'format': format
    }
    return await upload_instance(map_url, body, path)


async def upload_gen(path, project_id):
    """Upload generalization file."""
    gen_url = OSLSM_ENDPOINT + f'/v2/{project_id}/sim/lsm/parameters/'
    name, sha256 = path.name, get_sha256(path)
    body = {
        'file': {'sha256': sha256, 'filename': name},
    }
    return await upload_instance(gen_url, body, path)


def get_sha256(path):
    """Get file sha256."""
    with path.open('rb') as file:
        hash_obj = hashlib.sha256()
        while chunk := file.read(1024):
            hash_obj.update(chunk)
    return hash_obj.hexdigest()


async def create_instance(ossm_url, body):
    """Create scenario instance."""
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    async with aiohttp.ClientSession(
        connector=TCPConnector(verify_ssl=VERIFY_SSL)
    ) as sess:
        async with sess.post(
            ossm_url, data=json.dumps(body), headers=headers
        ) as resp:
            if resp.status < 400:
                res_dict = await resp.json()
                if 'file' in res_dict:
                    return (
                        res_dict["id"],
                        res_dict["url"],
                        res_dict["file"]["ready"],
                        res_dict["file"]["put_url"],
                        res_dict["file"]["sha256"],
                    ) 
                else:
                    return (
                        res_dict["id"],
                        res_dict["url"],
                        None,
                        None,
                        None,
                    )
            return None, None, None, None, None


async def update_status(ossm_url):
    """Update scenario instance status."""
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    body = {'ready': True}
    async with aiohttp.ClientSession(
        connector=TCPConnector(verify_ssl=VERIFY_SSL)
    ) as sess:
        async with sess.patch(
            ossm_url, data=json.dumps(body), headers=headers
        ) as resp:
            return resp.status < 400


async def create_category(body):
    """Create category."""
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    category_url = OSLSM_ENDPOINT + f'/v2/{PROJECT_ID}/sim/lsm/categories/'

    async with aiohttp.ClientSession(
        connector=TCPConnector(verify_ssl=VERIFY_SSL)
    ) as sess:
        async with sess.post(
            category_url, data=json.dumps(body), headers=headers
        ) as resp:
            if resp.status >= 400:
                raise UploadScenarioToOssmError('Create category failed.')
            res_dict = await resp.json()
            return res_dict.get("url")


async def get_category(params):
    """Get category."""
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    category_url = OSLSM_ENDPOINT + f'/v2/{PROJECT_ID}/sim/lsm/categories/'

    async with aiohttp.ClientSession(
        connector=TCPConnector(verify_ssl=VERIFY_SSL)
    ) as sess:
        async with sess.get(
            category_url, params=params, headers=headers
        ) as resp:
            if resp.status >= 400:
                raise UploadScenarioToOssmError('Get category failed.')
            res_dict = await resp.json()
            return res_dict


async def create_group(body):
    """Create group."""
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    group_url = OSLSM_ENDPOINT + f'/v2/{PROJECT_ID}/sim/lsm/groups/'

    async with aiohttp.ClientSession(
        connector=TCPConnector(verify_ssl=VERIFY_SSL)
    ) as sess:
        async with sess.post(
            group_url, data=json.dumps(body), headers=headers
        ) as resp:
            if resp.status >= 400:
                raise UploadScenarioToOssmError('Create group failed.')
            res_dict = await resp.json()
            return res_dict.get("id")


async def put_group(group_id, scenario_id):
    """put scenario into group."""
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    group_url = OSLSM_ENDPOINT + f'/v2/{PROJECT_ID}/sim/lsm/groups/{group_id}/scenarios/{scenario_id}/'

    async with aiohttp.ClientSession(
        connector=TCPConnector(verify_ssl=VERIFY_SSL)
    ) as sess:
        async with sess.put(
            group_url, headers=headers
        ) as resp:
            if resp.status >= 400:
                raise UploadScenarioToOssmError('Put scenarios to group failed.')
            return True


if __name__ == "__main__":
    main()
