import os
import hashlib
import json
import aiohttp
import asyncio
import time
import requests

from argparse import ArgumentParser
from dataclasses import dataclass
from pathlib import Path
from typing import Optional, List
from aiohttp import TCPConnector

token = ''
# Environment variable.
OSSM_ENDPOINT = os.getenv('OSSM_ENDPOINT', 'default')
PROJECT_ID = os.getenv('PROJECT_ID', 'default')
VIAM = os.getenv('VIAM', 'default')
MAX_TASK_SIZE = os.getenv('MAX_TASK_SIZE', 1)
VERIFY_SSL = os.getenv('VERIFY_SSL', False)
tasks = []
proxies = {
    "http": None,
    "https": None,
}


# Exception class
class ObusException(Exception):
    """Provide the base exception for obus."""


class UploadScenarioToOssmError(ObusException):
    """Upload scenario file to ossm error."""


class AssignedPathNotOneError(ObusException):
    """The assigned path of scenrio files is not equal to one."""


class CategoryExistException(Exception):
    """Provide the exception for category."""


class CreateFailedException(Exception):
    """Provide the exception for create."""


class NoSuchFileException(Exception):
    """Provide the exception for file download."""


# model
@dataclass
class ScenarioPath:
    """The four paths of scenario."""

    scenario: Optional[Path]
    map: Optional[Path]
    osgb: Optional[Path]

    def __bool__(self):
        return self.scenario is not None and self.map is not None


def main(argv=None):
    """Octopus Batch Upload Scenario."""
    parser = ArgumentParser()
    parser.add_argument('-V', '--version', action='version')
    parser.description = main.__doc__
    parser.set_defaults(func=lambda _: 1)
    subparsers = parser.add_subparsers(title='subcommands')
    add_parser_upload_to(subparsers)
    add_parser_login_to(subparsers)
    add_parser_download_to(subparsers)
    args = parser.parse_args(argv)
    status = args.func(args)
    raise SystemExit(status)


def add_parser_login_to(subparsers):
    """Add sub command `login` to main parser."""
    cmd: ArgumentParser = subparsers.add_parser(
        'login',
        aliases='l',
        help=login_from_cli.__doc__,
    )
    cmd.description = login_from_cli.__doc__
    cmd.set_defaults(func=login_from_cli)

    #认证用的ak和sk硬编码到代码中或者明文存储都有很大的安全风险，建议在配置文件或者环境变量中密文存放，使用时解密，确保安全；
    #本示例以ak和sk保存在环境变量中来实现身份验证为例，运行本示例前请先在本地环境中设置环境变量HUAWEICLOUD_SDK_AK和HUAWEICLOUD_SDK_SK。 
    #如果进行了加密还需要进行解密操作。
    cmd.add_argument(
        '-ak',
        required=True,
        help='The access key your account.'
    )
    cmd.add_argument(
        '-sk',
        required=True,
        help='The secret key of your account.'
    )


def login_from_cli(args) -> 0:
    """Login in platform with username and password."""
    _login(args.ak, args.sk)
    return 0


def _login(ak, sk):
    data = {'ak': ak, "sk": sk}
    headers = {'Content-Type': 'application/json'}
    resp = requests.post(url=VIAM, json=data, verify=False, headers=headers)
    resp.raise_for_status()
    token_ = json.loads(resp.content)['accessToken']
    with open('/tmp/token.txt', 'w') as file:
        file.write(token_)


def add_parser_upload_to(subparsers):
    """Add sub command `upload` to main parser."""
    cmd: ArgumentParser = subparsers.add_parser(
        'upload',
        aliases='u',
        help=upload_from_cli.__doc__,
    )
    cmd.description = upload_from_cli.__doc__
    cmd.set_defaults(func=upload_from_cli)

    cmd.add_argument(
        '--path',
        '-p',
        type=Path,
        nargs='+',
        help='The directory of scenario files.'
    )
    cmd.add_argument(
        '--version',
        '-v',
        default='vtd',
        help='The version of dynamic scenario files, default is vtd.'
    )
    cmd.add_argument(
        '--root',
        '-r',
        help='The root of scenario category, must not exist.'
    )
    cmd.add_argument(
        '--simulator',
        '-s',
        default='VTD',
        help='The simulator type of scenario category and group, VTD or SimPro.'
    )


def add_parser_download_to(subparsers):
    """Add sub command `download` to main parser."""
    cmd: ArgumentParser = subparsers.add_parser(
        'download',
        aliases='d',
        help=download_task_report.__doc__,
    )
    cmd.description = download_task_report.__doc__
    cmd.set_defaults(func=download_task_report)

    cmd.add_argument(
        '--path',
        '-p',
        type=Path,
        nargs='+',
        help='The directory of local dir.'
    )
    cmd.add_argument(
        '--id',
        '-i',
        help='The id of batch task.'
    )


def download_task_report(args) -> int:
    """Download task report from octopus."""
    start = time.time()
    get_token()
    asyncio.run(_download(args.path, args.id))
    print(f'total time is {time.time() - start}')
    return 0


async def _download(path: List[Path], batch_id: int):
    local_path = path[0]
    count, simulations = await get_simulation_info(PROJECT_ID, batch_id, 1, MAX_TASK_SIZE)
    totalPage = int(count / MAX_TASK_SIZE)
    for i in range(totalPage+1):
        _, simulations = await get_simulation_info(PROJECT_ID, batch_id, i+1, MAX_TASK_SIZE)
        for simulation in simulations:
            algorithm_path = local_path / f'sim-{batch_id}-{simulation["id"]}-{simulation["scenario"]["scenario_resource_name"]}.alg.log'
            osi_path = local_path / f'sim-{batch_id}-{simulation["id"]}-{simulation["scenario"]["scenario_resource_name"]}.osi.pb'
            await get_file(PROJECT_ID, simulation["id"], 'algorithm_log', algorithm_path)
            await get_file(PROJECT_ID, simulation["id"], 'sim_osi', osi_path)
    return
    

async def get_simulation_info(project_id, simulation_id, page, page_size):
    simulation_url = OSSM_ENDPOINT + f'/v2/{project_id}/sim/pm/simulations/?page={page}&page_size={page_size}&batch_id={simulation_id}'
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    async with aiohttp.ClientSession() as sess:
        async with sess.get(
                simulation_url, headers=headers, ssl=False
        ) as resp:
            if resp.status < 400:
                res_dict = await resp.json()
                count = res_dict['count']
                if res_dict['results']:
                    return count, res_dict['results']
                return count, []
            if resp.status == 404:
                return None, []
            raise
        

async def get_file(project_id, simulation_id, type, file_path):
    simulation_url = OSSM_ENDPOINT + f'/v2/{project_id}/sim/pm/simulations/{simulation_id}/files/?type={type}'
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    async with aiohttp.ClientSession() as sess:
        async with sess.get(
                simulation_url, headers=headers, ssl=False
        ) as resp:
            if resp.status < 400:
                with open(file_path, 'wb') as fp:
                    res_dict = await resp.json()
                    res = requests.get(url=res_dict[str(type)+'_url'], proxies=proxies)
                    fp.write(res.content)
                    return
            raise NoSuchFileException(f'Please check if file exist, simulation_id:{simulation_id}, type:{type}')


def get_token():
    global token
    with open('/tmp/token.txt', 'r') as file:
        token = file.read()


def upload_from_cli(args) -> int:
    """Upload scenario files from cli command."""
    start = time.time()
    get_token()
    asyncio.run(_upload(args.path, args.version, args.root, args.simulator))
    print(f'total time is {time.time() - start}')
    return 0


async def _upload(path: List[Path], version: str, root: str, simulator: str):
    """Create categories and groups then upload scenario files."""
    count = 0
    root_category = await create_category(PROJECT_ID, root, None)
    await __upload(path[0], version, root_category, simulator, None)
    if tasks:
        total = len(tasks)
        batch = int(total / MAX_TASK_SIZE)
        for i in range(batch):
            task = tasks[i:i+MAX_TASK_SIZE]
            res = await asyncio.gather(*task)
            count += _update_count(res)
            print(f'The uploaded scenario number is {count} / {total}')
        if total > batch*MAX_TASK_SIZE:
            task = tasks[batch*MAX_TASK_SIZE:]
            res = await asyncio.gather(*task)
            count += _update_count(res)
            print(f'The uploaded scenario number is {count} / {total}')
    return count


def _update_count(res):
    count = 0
    for i in res:
        if i:
            count += 1
    return count


def is_null(cur_dir):
    """判断当前文件夹是否为空文件夹."""
    children = []
    for item in cur_dir.iterdir():
        children.append(item)
    if len(children) == 0:
        return True
    return False


def is_scenario(cur_dir):
    """判断当前文件夹是否为场景，即当前文件夹下只有文件，没有其他文件夹."""
    for item in cur_dir.iterdir():
        if os.path.isdir(item):
            return False
    return True


def is_group(cur_dir):
    """判断当前文件夹是否为场景库，即当前文件夹下只有场景."""
    for item in cur_dir.iterdir():
        if not is_scenario(item):
            return False
    return True


async def __upload(path: Path, version: str, parent_category_id: int, simulator: str, group_id):
    """Create categories and groups then upload scenario files."""
    name = path.stem
    if is_null(path):
        pass
    elif is_scenario(path):
        if group_id is None:
            raise ValueError("Scenario can't be patched to category.")
        tasks.append(_upload_files(path, version, PROJECT_ID, group_id, simulator))
    elif is_group(path):
        group_id = await create_group(PROJECT_ID, name, parent_category_id, simulator)
        for child in sorted(path.absolute().iterdir()):
            await __upload(child, version, parent_category_id, simulator, group_id)
    else:
        category_id = await create_category(PROJECT_ID, name, parent_category_id)
        for child in sorted(path.absolute().iterdir()):
            await __upload(child, version, category_id, simulator, group_id)
    return


async def _upload_files(path: Path, version: str, project_id, group_id, simulator) -> bool:
    """Upload scenario files of a scenario directory."""

    scenario_path = ScenarioPath(None, None, None)
    scenario, map, osgb = 0, 0, 0
    for child in path.iterdir():
        if child.suffix == '.xosc' or child.suffix == '.xml':
            scenario += 1
            scenario_path.scenario = child
        elif child.suffix == '.xodr':
            map += 1
            scenario_path.map = child
        elif child.suffix == '.osgb':
            osgb += 1
            scenario_path.osgb = child
    if scenario > 1 or map > 1 or osgb > 1:
        print(f'upload failed, {path.name} directory has same name files.')
        return False
    if scenario_path:
        scenario_id = await upload_all_instance(
            path=scenario_path,
            project_id=PROJECT_ID,
            simulator=simulator,
            description='',
            version=version
        )
        await scenario_to_group(project_id, group_id, scenario_id)
        return True
    print(f'upload failed, {path.name} directory lack of necessary files.')
    return False


async def upload_all_instance(
    path: ScenarioPath, project_id, simulator, description, version
):
    """Upload all scenario instance."""
    model_url, model_status = None, True
    if path.osgb:
        model_url, model_status, _ = await upload_model(path.osgb, project_id)
    if model_status:
        map_url, map_status, _ = await upload_map(path.map, project_id)
        if map_status:
            instance_url, scenario_status, instance_id = await upload_scenario(
                path.scenario, project_id, model_url, map_url,
                (simulator, description, version)
            )
            if scenario_status:
                if await update_scenario_status(project_id, instance_id):
                    # _, url, _, _ = await create_case(
                    #     path.scenario, project_id, instance_url,
                    #     (simulator, description),
                    # )
                    return instance_id
            raise UploadScenarioToOssmError('Upload scenario to ossm error')
        raise UploadScenarioToOssmError('Upload map to ossm error')
    raise UploadScenarioToOssmError('Upload model to ossm error')


async def upload_scenario(path, project_id, model_url, map_url, detail):
    """Upload scenario."""
    scenario_url = OSSM_ENDPOINT + f'/v2/{project_id}/sim/sm/scenarios/'
    sha256 = get_sha256(path)
    body = {
        'description': {
            'content': detail[1]
        },
        'file': {
            'sha256': sha256,
            'filename': path.name
        },
        'version': detail[2],
        'simulator': detail[0],
        'name': path.parent.name,
        'priority': 120,
        'model': model_url,
        'map': map_url
    }
    return await upload_instance(scenario_url, body, path)


async def create_case(path, project_id, scenario_url, detail):
    """Create test case."""
    case_url = OSSM_ENDPOINT + f'/v2/{project_id}/sim/sm/testcases/'
    body = {
        'description': {
            'content': detail[1]
        },
        'simulator': detail[0],
        'name': path.parent.stem,
        'priority': 120,
        'scenario': scenario_url,
        'scenario_status': 1,
        'status': 10
    }
    return await create_instance(case_url, body)


async def update_scenario_status(project_id, instance_id):
    """Update scenario status to available."""
    scenario_url = OSSM_ENDPOINT + f'/v2/{project_id}/sim/sm/scenarios/{instance_id}/'
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    body = {'status': 0}
    async with aiohttp.ClientSession() as sess:
        async with sess.patch(
                scenario_url, data=json.dumps(body), headers=headers, ssl=False
        ) as resp:
            return resp.status < 400


async def upload_file(path, put_url):
    """Upload scenario instance file."""
    headers = {'Content-Type': 'application/octet-stream'}
    with path.open('rb') as file:
        async with aiohttp.ClientSession() as sess:
            async with sess.put(
                put_url, data=file, headers=headers, ssl=False
            ) as resp:
                return resp.status < 400


async def upload_instance(instance_url, body, path):
    """Handel model or map or scenario."""
    instance_id, url, ready, put_url = await create_instance(instance_url, body)
    if ready:
        return url, True, instance_id
    if not put_url:
        raise Exception('Ossm has some error')
    if await upload_file(path, put_url):
        status_url = instance_url + f'{instance_id}/'
        if await update_status(status_url):
            return url, True, instance_id
    return url, False, instance_id


async def upload_model(path, project_id):
    """Upload model."""
    model_url = OSSM_ENDPOINT + f'/v2/{project_id}/sim/sm/models/'
    name, sha256 = path.name, get_sha256(path)
    body = {'file': {'sha256': sha256, 'filename': name}, 'format': 'osgb'}
    return await upload_instance(model_url, body, path)


async def upload_map(path, project_id):
    """Upload map."""
    map_url = OSSM_ENDPOINT + f'/v2/{project_id}/sim/sm/maps/'
    name, sha256 = path.name, get_sha256(path)
    body = {'file': {'sha256': sha256, 'filename': name}, 'version': 'v1.6.0'}
    return await upload_instance(map_url, body, path)


def get_sha256(path):
    """Get file sha256."""
    with path.open('rb') as file:
        hash_obj = hashlib.sha256()
        while chunk := file.read(1024):
            hash_obj.update(chunk)
    return hash_obj.hexdigest()


def check_if_scenario_instance(path: Path):
    """Check dir if scenario instance"""
    scenario, map, osgb = 0, 0, 0
    for child in path.iterdir():
        if child.suffix == '.xosc' or child.suffix == '.xml':
            scenario += 1
        elif child.suffix == '.xodr':
            map += 1
        elif child.suffix == '.osgb':
            osgb += 1
    if scenario == 0 or map == 0:
        return False
    if scenario > 1 or map > 1 or osgb > 1:
        return False
    return True


async def check_category_if_exist(project_id, name, parent: Optional[int]):
    """Check category if exist."""
    category_get_url = OSSM_ENDPOINT + f'/v2/{project_id}/sim/sm/categories/?name={name}'
    if parent:
        category_get_url = category_get_url + f'&parent={parent}'
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    async with aiohttp.ClientSession() as sess:
        async with sess.get(
            category_get_url, headers=headers, ssl=False
        ) as resp:
            if resp.status < 400:
                res_dict = await resp.json()
                if res_dict['results']:
                    return True, res_dict['results'][0]['id']
            return False, None


async def create_category(project_id, name, parent: Optional[int]):
    """Create category if not exist, if exist return category id."""
    if_exist, category_id = await check_category_if_exist(project_id, name, parent)
    if not parent and if_exist:
        raise CategoryExistException('Root category already exist')
    if parent and if_exist:
        return category_id
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    category_create_url = OSSM_ENDPOINT + f'/v2/{project_id}/sim/sm/categories/'
    body = {'name': name}
    if parent:
        body['parent'] = f'/categories/{parent}/'
    async with aiohttp.ClientSession() as sess:
        async with sess.post(
            category_create_url, data=json.dumps(body), headers=headers, ssl=False
        ) as resp:
            if resp.status < 400:
                res_dict = await resp.json()
                return res_dict['id']
            raise CreateFailedException('Create category failed')


async def check_group_if_exist(project_id, name, parent: int):
    """Check group if exist."""
    category_get_url = OSSM_ENDPOINT + f'/v2/{project_id}/sim/sm/groups/?name={name}&parent={parent}'
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    async with aiohttp.ClientSession() as sess:
        async with sess.get(
            category_get_url, headers=headers, ssl=False
        ) as resp:
            if resp.status < 400:
                res_dict = await resp.json()
                if res_dict['results']:
                    return True, res_dict['results'][0]['id']
            return False, None


async def create_group(project_id, name, category: int, simulator: str):
    """Create group if not exist, if exist return group id."""
    if_exist, group_id = await check_group_if_exist(project_id, name, category)
    if if_exist:
        return group_id
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    category_create_url = OSSM_ENDPOINT + f'/v2/{project_id}/sim/sm/groups/'
    body = {'name': name, 'parent': f'/v2/{project_id}/sim/sm/categories/{category}/',
            'simulator': simulator, 'description': {'content': ''}}
    async with aiohttp.ClientSession() as sess:
        async with sess.post(
            category_create_url, data=json.dumps(body), headers=headers, ssl=False
        ) as resp:
            if resp.status < 400:
                res_dict = await resp.json()
                return res_dict['id']
            raise CreateFailedException('Create category failed')


async def scenario_to_group(project_id, group_id, scenario_id):
    """Add scenario to group"""
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    scenario_to_group_url = OSSM_ENDPOINT + f'/v2/{project_id}/sim/sm/groups/{group_id}/scenarios/{scenario_id}/'
    async with aiohttp.ClientSession() as sess:
        async with sess.put(
            scenario_to_group_url, headers=headers, ssl=False
        ) as resp:
            if resp.status < 400:
                return True
            return False


async def create_instance(ossm_url, body):
    """Create scenario instance."""
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    async with aiohttp.ClientSession(
        connector=TCPConnector(verify_ssl=VERIFY_SSL)
    ) as sess:
        async with sess.post(
            ossm_url, data=json.dumps(body), headers=headers
        ) as resp:
            if resp.status < 400:
                res_dict = await resp.json()
                if 'file' in res_dict:
                    return (
                        res_dict['id'],
                        res_dict['url'],
                        res_dict['file']['ready'],
                        res_dict['file']['put_url'],
                    )
                else:
                    return (
                        res_dict['id'],
                        res_dict['url'],
                        None,
                        None,
                    )
            return None, None, None, None


async def update_status(ossm_url):
    """Update scenario instance status."""
    headers = {'Content-Type': 'application/json', 'Authorization': token}
    body = {'file': {'ready': True}}
    async with aiohttp.ClientSession(
        connector=TCPConnector(verify_ssl=VERIFY_SSL)
    ) as sess:
        async with sess.patch(
            ossm_url, data=json.dumps(body), headers=headers
        ) as resp:
            return resp.status < 400

if __name__ == "__main__":
    main()

