# huawei-octopus scenario-upload demo

该样例实现了通过脚本批量上传本地场景文件到八爪鱼仿真场景库的功能。样例同时补充了算法日志，osi.pb批量文件下载的功能

## 脚本使用前提

- 获取到账号对应的ak，sk。
- 本地准备好需要上传的场景。

## 使用方法

- 使用ak，sk登录
```
python main.py login -ak xxx -sk xxx（xxx填入ak，sk真值）
```
- 使用批量上传场景功能
```
python main.py upload --path /tmp --version vtd --root xxx --simulator VTD
```

| 参数名    | 数据类型 | 是否必选 | 说明                                                 |
| --------- | -------- | -------- | ---------------------------------------------------- |
| path      | String   | 是       | 要上传的批量场景本地文件夹地址                       |
| root      | String   | 是       | 根场景库分类，如果与已有的根场景库分类重名，将会报错 |
| version   | String   | 否       | 场景版本，默认vtd                                    |
| simulator | String   | 否       | 默认为VTD，支持VTD和SimPro两种仿真器                 |

- 使用批量下载功能

```
python main.py download --path /tmp --id xxx
```

| 参数名 | 数据类型 | 是否必选 | 说明                                     |
| ------ | -------- | -------- | ---------------------------------------- |
| path   | String   | 是       | 要下载到的本地文件夹位置                 |
| id     | String   | 是       | 需要下载算法日志和osi.pb文件的批量任务id |